var express = require('express');
var http = require('http');
var url = require('url');
var app = express();

//Get the port of the application
var port = process.env.PORT || 8080;

var apiURL = "ibl.api.bbci.co.uk";

app.use(express.static(__dirname + '/public'));


//configure node to use ejs templating
app.set('view engine', 'ejs');


//ROUTING CONFIG

//index 
app.get('/', function(req, res) {
    res.render('pages/index');
});

//listing 
app.get('/listing/:letter', function(req, res) {

	//Get the letter requested
	var letter = req.params.letter;

	//Get the page requested
	var query = url.parse(req.url,true).query;
	var page = parseInt(query.page);

	//Default paging to 1 if not specified
	if(isNaN(page)) {
		page = 1;
	}


	//AJAX call to BBC API
	http.get({
        host: apiURL,
        path: '/ibl/v1/atoz/' + letter + '/programmes?page=' + page
    }, function(response) {

		var body = '';
        response.on('data', function(d) {
            body += d;
        });

        response.once('end', function() {

            try {

            	//Parse the JSON
                var listings = JSON.parse(body);

            	//Set the records per page and total nubmer of records
            	var perPage = listings.atoz_programmes.per_page;
            	var records = listings.atoz_programmes.count;

        		//Render the page to the browser
                res.render('pages/listing', {
			    	letter: letter,
		    		listings: listings.atoz_programmes.elements,
		    		totalPages: Math.ceil(records/perPage),
		    		page: page
			    });
            } catch (err) {
            	//Throw 404 error
                console.error('Cannot load listing', err);
                res.status(404).send('Not found');
            }

        });

	});


    
});

//END ROUTING CONFIG


//start web server
app.listen(port, function() {});
