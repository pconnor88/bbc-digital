Source Code Location: https://bitbucket.org/pconnor88/bbc-digital/src
Application Location: https://bbc-iplayer-test-project.herokuapp.com

Language used: NodeJS

Comments
========

Unfortunately I only saw this job posted yesterday (day before the job expiry date) so have not been able to allocate the 3 days of recommended time to the project.
In the brief there is a heavy emphasis on unit testing and TDD, but due to the short timescale and me having no prior experience in NodeJS or Heroku before the start of
this project I simply run out of time.

From online reading, I would use supertest (https://www.npmjs.com/package/supertest) and mochajs (http://mochajs.org/) to create and run my unit tests.
I will continue to update this project even after submission as I am keen to learn more.